﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BankApp.Models;
using System.Web.Security;

namespace BankApp.Controllers
{
    public class HomeController : Controller
    {
        UserDbEntities dbhelper=new UserDbEntities();

        [Authorize] 
        public ActionResult Index()
        {
            return View("Index",dbhelper.UserTbs.ToList());
        }

        public ActionResult Add()
        {
            return View("ApplicantView");
        }

        public ActionResult AfterCreate(UserTb userToBeAdded)
        {
            dbhelper.UserTbs.Add(userToBeAdded);
            int rowsAffected = dbhelper.SaveChanges();
            if(rowsAffected>0)
            {
                return Redirect("/Home/Index");
            }
            return View("/Home/Add");
        }
        public ActionResult Approve(int id)
        {
            UserTb userToBeApproved=dbhelper.UserTbs.Find(id);
            return View("Approve",userToBeApproved);
        }

        public ActionResult AfterApprove(UserTb UserToBeApproved)
        {
            UserTb userApproved = dbhelper.UserTbs.Find(UserToBeApproved.UserNo);
            userApproved.Status = UserToBeApproved.Status;
            userApproved.Role = UserToBeApproved.Role;
            int rowsAffected = dbhelper.SaveChanges();
            if(rowsAffected>0)
            {
                return Redirect("/Home/Index");
            }
            return Redirect("/Home/Approve");

        }

        public ActionResult Login()
        {
            return View("Login");
        }

        public ActionResult AfterLogin(UserTb user,String returnurl)
        {
            if(Checkwithdb(user))
            {
                FormsAuthentication.SetAuthCookie(user.UserName, false);
                if(returnurl!=null)
                {
                    return Redirect(returnurl);
                }
                else
                {
                    return Redirect("/Home/Index");
                }
            }
            else
            {
                ViewBag.message = "UserName or Password Incoorect Please Check";
                return View("Login");
            }
        }

        private bool Checkwithdb(UserTb usertocheck)
        {
            foreach(var item in dbhelper.UserTbs)
            {
                if(item.email==usertocheck.email&&item.Password==usertocheck.Password)
                {
                    return true;
                }
            }
            return false;
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return View("Login");
        }


    }
}