CREATE TABLE UserTb(
UserNo INT PRIMARY KEY IDENTITY,
UserName VARCHAR(30),
Age INT,
Address VARCHAR(50),
ContactNumber Varchar(10),
Status VARCHAR(20),
Role VARCHAR(20),
email VARCHAR(30),
Password VARCHAR(30)
);

Database Name :-UserDb

INSERT INTO UserTb(UserName,Age,Address,ContactNumber,Status,Role,email,Password)
VALUES
('Rohit',35,'MUMBAI',985024126,'Approved','Manager','ro45@gmail.com','1234'),
('Virat',35,'Delhi',995024126,'PROCESSING','Applicant','vk18@gmail.com','1234'),
('Rishabh',26,'Delhi',982024126,'PROCESSING','Applicant','rishabh@gmail.com','1234'),
('jasprit',30,'MUMBAI',985023126,'PROCESSING','Applicant','jas@gmail.com','1234'),
('Ruturaj',22,'Pune',985024248,'PROCESSING','Applicant','ruturaj@gmail.com','1234');



